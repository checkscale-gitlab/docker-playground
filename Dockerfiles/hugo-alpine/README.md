# hugo-alpine

Runs the [Hugo](https://gohugo.io) static site generator on a [`go:alpine` base](https://hub.docker.com/_/golang/), using [the Copr installer](https://copr.fedorainfracloud.org/coprs/daftaupe/hugo/). 
This approach will take longer to build and consume more diskspace and may result in an unstable version of Hugo.

Benchmarks of hugo-alpine (Alpine + go install) vs
hugo-fedora (Fedora + DNF / Copr install).

| Name        | Size   | Build time |
| ----------- | ------ | ---------- |
| hugo-alpine | 659 MB | 3:59.08    |
| hugo-fedora | 509 MB | 1:05.96    |

A demo site is added in the `app` directory, using [the Hugo Material Docs theme](https://github.com/digitalcraftsman/hugo-material-docs/) and it's [`ExampleSite`](https://github.com/digitalcraftsman/hugo-material-docs/tree/master/exampleSite). The site is served, on the container and to the host machine, over port `1313`, i.e. [http://localhost:1313](http://localhost:1313).

## Build

````
docker build -t hugo-alpine .
````

## Run

Run undetached, exposing the hugo watcher output:

````
docker run -it -p 1313:1313 --mount type=bind,source=`pwd`/app,destination=/app hugo-alpine
````

Run detached:

````
docker run -dit -p 1313:1313 --mount type=bind,source=`pwd`/app,destination=/app hugo-fedora
````

## Exec

````
docker exec -it `docker ps -q` /bin/bash 
````

## Logs

Tail the `hugo watch` output on a detached container:

````
docker logs -f `docker ps -q`
````