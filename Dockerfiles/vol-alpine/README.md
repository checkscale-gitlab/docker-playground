# vol-alpine

Run Alpine based container, with info on running a container with a volume.

## Build

````
docker build -t vol-alpine .
````

## Run

````
docker run \
 -dit \
 --name vol-alpine-$(date +%Y-%m-%d--%H-%M) \
 --mount source=vol-alpine-data,destination=/opt/data \
 vol-alpine
````

As one-liner:

````
docker run -dit --name vol-alpine-$(date +%Y-%m-%d--%H-%M) --mount source=vol-alpine-data,destination=/opt/data vol-alpine
````

## Exec

````
docker exec -it $(docker ps -q) /bin/bash
````

## Misc info

* `WORKDIR` is defined to the mount destination, `/opt/data`, running `docker exec` will load you straight into the mount on the container.
* On the host, the data in the mount is available at `/var/lib/docker/volumes/vol-alpine-data/_data`.
* Create a file on the host and run it from the container:
  * `echo "this is a host created file" > /var/lib/docker/volumes/vol-alpine-data/_data/host`
  * `docker exec -it $(docker ps -q) cat /opt/data/host `
* Create a file on the container and read it from the host:
  * Connect to the container: `docker exec -it $(docker ps -q) /bin/bash`
  * Create the file with content: `echo "this is a container created file" > container`
  * Exit the container: `exit`
  * Read the file on the host: `cat /var/lib/docker/volumes/vol-alpine-data/_data/container`

