# Docker Playground

Collection of experimental and PoC `Docker` stuff.
Refer to the README's in the sub-dirs for details.

## Attribution

* includes [deviantony/docker-elk](https://github.com/deviantony/docker-elk) as submodule
