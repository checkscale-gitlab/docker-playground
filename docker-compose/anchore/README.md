# Anchore

```bash
./setup.sh
docker-compose up -d
```

Check the status.

```bash
docker-compose exec anchore-engine anchore-cli --u admin --p foobar system status
```

Check data feed sync.

```bash
docker-compose exec anchore-engine anchore-cli --u admin --p foobar system feeds list
```

# Resources

* [Install with docker-compose docs](https://anchore.freshdesk.com/support/solutions/articles/36000020729-install-with-docker-compose)
* [anchore-cli](https://github.com/anchore/anchore-cli)
